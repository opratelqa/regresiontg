package test;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

//import pages.TestPage;
import pages.*;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.PDFont;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import test.ManipularExcel;

	public class Test29CMSPermisos extends BasePage {

	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario ;
	String paswd ; 
	

	/*
	int Loop = ManipularExcel.MAXIMO_FILAS;
	@DataProvider(name = "filas")
    private Object[] createData1() {
	 ArrayList<String> filas = new ArrayList<>();
		for(int i=3; i<= Loop; i++) {
			String C = String.valueOf(i);
			filas.add(C);
		}
	       return filas.toArray();
    }
    */  

	@BeforeClass
	public void IngresoAmbiente() {
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 10);
		TestBase TB = new TestBase(driver);
		TB.UrlDeLaWeb(apuntaA);
		
		
	}	
	
	@AfterMethod
	public void IrHome(ITestResult result) {
		
		if(ITestResult.FAILURE==result.getStatus()) 
		{ 
			BasePage.captureScreenshot(driver, BasePage.base__fechaActual("kk_mm_ss")); 
		}		 
		TestBase TB = new TestBase(driver); 
		TB.UrlDeLaWeb(apuntaA);
	}
	
	@AfterClass
	public void CloseChrome() {
		driver.quit();
		
	}   

	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}
	}
	

	
	/*************************************************************/
	/****************    SELECCIONA USUARIO    *******************/
	/*************************************************************/ 
	
	
	
	
	String chooseYourUser ="it@opratel.com"; //Usuario Premium	
	//String chooseYourUser ="comprovidanueva@gmail.com"; //Usuario No Premium
	
	
	String passAmbiente ="123456"; //Contraseña QA it@opratel.com 
	//String passAmbiente ="t0g4.0pr4"; //Contraseña PRODUCCION
	//String passAmbiente ="tg*0pr4t31"; //Contraseña DV4
	
	
	
	
	/*************************************************************/
	/************   SELECCION A QUE AMBIENTE APUNTA  *************/
	/*************************************************************/ 
	
	String ambiente = "todosgamers.com";
	
	
	String apuntaA ="http://qa."; //AMBIENTE QA
	
	//String apuntaA ="http://dev4."; //AMBIENTE DEV7
	//String apuntaA ="http://"; //AMBIENTE PRODUCCION
	
	
	
	
	/*************************************************************/
	/*************************************************************/
	/*************************************************************/
	//@Test (priority=60) ROMPE deben arreglar ruta de cms matchmaking
	@Test (priority=62)
	public void CMSpermisos() {
	EDPTG77CMSPermisos TP65 = new EDPTG77CMSPermisos(driver);
	TP65.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP65.CMSPermisos(apuntaA);
		TP65.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=63)
	public void CMSpermisosCrear() {
	EDPTG78CMSPermisosCrear TP66 = new EDPTG78CMSPermisosCrear(driver);
	TP66.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP66.CMSPermisos(apuntaA); 
			TP66.CMSPermisosCrear();
			TP66.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=64)
	public void CMSpermisosEditar() {
	EDPTG79CMSPermisosEditar TP67 = new EDPTG79CMSPermisosEditar(driver);
	TP67.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP67.CMSPermisos(apuntaA);
			TP67.CMSPermisosEditar(); 
			TP67.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=65)
	public void CMSpermisosEliminar() {
	EDPTG80CMSPermisosEliminar TP68 = new EDPTG80CMSPermisosEliminar(driver);
	TP68.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP68.CMSPermisos(apuntaA);
			TP68.CMSPermisosEliminar();
			TP68.LogOutTodosGamers(apuntaA);
	}
		
}
	
	
	/*Jenkins: http://54.163.143.224:8080/ 
	 * Usuario: qaauttg
	 * Contraseña: t0g4.0pr4
	 * */
