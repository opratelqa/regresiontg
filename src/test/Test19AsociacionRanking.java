package test;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

//import pages.TestPage;
import pages.*;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.PDFont;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import test.ManipularExcel;

	public class Test19AsociacionRanking extends BasePage {

	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario ;
	String paswd ; 
	

	/*
	int Loop = ManipularExcel.MAXIMO_FILAS;
	@DataProvider(name = "filas")
    private Object[] createData1() {
	 ArrayList<String> filas = new ArrayList<>();
		for(int i=3; i<= Loop; i++) {
			String C = String.valueOf(i);
			filas.add(C);
		}
	       return filas.toArray();
    }
    */  

	@BeforeClass
	public void IngresoAmbiente() {
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 10);
		TestBase TB = new TestBase(driver);
		TB.UrlDeLaWeb(apuntaA);
		
		
	}	
	
	@AfterMethod
	public void IrHome(ITestResult result) {
		
		if(ITestResult.FAILURE==result.getStatus()) 
		{ 
			BasePage.captureScreenshot(driver, BasePage.base__fechaActual("kk_mm_ss")); 
		}		 
		TestBase TB = new TestBase(driver); 
		TB.UrlDeLaWeb(apuntaA);
	}
	
	@AfterClass
	public void CloseChrome() {
		driver.quit();
		
	}   

	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}
	}
	

	
	/*************************************************************/
	/****************    SELECCIONA USUARIO    *******************/
	/*************************************************************/ 
	
	
	
	
	String chooseYourUser ="it@opratel.com"; //Usuario Premium	
	//String chooseYourUser ="comprovidanueva@gmail.com"; //Usuario No Premium
	
	
	String passAmbiente ="123456"; //Contraseña QA it@opratel.com 
	//String passAmbiente ="t0g4.0pr4"; //Contraseña PRODUCCION
	//String passAmbiente ="tg*0pr4t31"; //Contraseña DV4
	
	
	
	
	/*************************************************************/
	/************   SELECCION A QUE AMBIENTE APUNTA  *************/
	/*************************************************************/ 
	
	String ambiente = "todosgamers.com";
	
	
	String apuntaA ="https://qa."; //AMBIENTE QA
	
	//String apuntaA ="https://dev4."; //AMBIENTE DEV7
	
	//String apuntaA ="https://"; //AMBIENTE PRODUCCION
	
	
	
	
	/*************************************************************/
	/*************************************************************/
	/*************************************************************/
	@Test (priority=38)
	public void CMSAsociacionR() {
	EDPTG45CMSAsociacionRanking TP39 = new EDPTG45CMSAsociacionRanking(driver);
	TP39.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP39.CMSAsociacionRanking(apuntaA);
		TP39.LogOutTodosGamers(apuntaA);
	}
}
	
	
	/*Jenkins: http://54.163.143.224:8080/ 
	 * Usuario: qaauttg
	 * Contraseña: t0g4.0pr4
	 * */
