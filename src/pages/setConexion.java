package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class setConexion {
	
	static WebDriver driver; 
	

	public static WebDriver setup() { 
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		//System.setProperty("webdriver.chrome.driver", "resources\\chromedriver.exe");
	    ChromeOptions options = new ChromeOptions(); 
	    options.addArguments("start-maximized"); 
	    options.addArguments("--verbose", "--log-path=/var/log/chromedriver.log");
	    options.addArguments("--headless", "window-size=1341,810", "--no-sandbox");
	    //options.addArguments("--no-sandbox");
	   // options.addArguments("--disable-dev-shm-usage");
	    driver = new ChromeDriver(options); 
	    return driver; 
	} 
	
}
