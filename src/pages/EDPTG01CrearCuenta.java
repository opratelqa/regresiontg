 package pages;

 
 import static org.testng.Assert.assertTrue;

 import java.nio.charset.CodingErrorAction;
 import java.util.ArrayList;
 import java.util.List;

 import javax.swing.JOptionPane;

 //@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
 //@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
 //private WebElement btnTramites;


 import javax.swing.ScrollPaneLayout;

 import org.openqa.selenium.Alert;
 import org.openqa.selenium.By;
 import org.openqa.selenium.JavascriptExecutor;
 import org.openqa.selenium.Keys;
 import org.openqa.selenium.WebDriver;
 import org.openqa.selenium.WebElement;
 import org.openqa.selenium.firefox.FirefoxDriver;
 import org.openqa.selenium.interactions.Actions;
 import org.openqa.selenium.support.FindBy;
 import org.openqa.selenium.support.How;
 import org.openqa.selenium.support.PageFactory;
 import org.openqa.selenium.support.ui.ExpectedConditions;
 import org.openqa.selenium.support.ui.Select;
 import org.openqa.selenium.support.ui.WebDriverWait;
 import org.testng.Assert;
 import org.testng.annotations.Test;
 import okhttp3.Call;
 import test.TestBaseTG;

 import java.io.BufferedInputStream;
 import java.io.File;
 import java.io.IOException;
 import java.io.InputStream;
 import java.net.URL;

 import test.TestBase;
 import test.ManipularExcel;
 import java.io.BufferedInputStream;
 import java.io.File;
 import java.io.IOException;
 import java.io.InputStream;
 import java.net.URL;
 import org.testng.Assert;
 import org.testng.annotations.Test;
 import okhttp3.Call;
 import test.TestBase;
 import test.ManipularExcel;
 import java.io.BufferedInputStream;
 import java.io.File;
 import java.io.IOException;
 import java.io.InputStream;
 import java.net.URL;


  
   
 public class EDPTG01CrearCuenta extends TestBaseTG {
 	
 	final WebDriver driver; 
 	public EDPTG01CrearCuenta(WebDriver driver){
 		this.driver = driver;
 		PageFactory.initElements(driver, this);
 	} 	 
 	
 	  
 	/*
 	 ******PASAR A BASEPAGE
 	 */
 	public void WaitForElementClickable(By element, int tiempo) {
 		WebDriverWait wait;
 		wait = new WebDriverWait(driver, tiempo);
 		wait.until(ExpectedConditions.elementToBeClickable(element));
 	}

 	public void WaitForElementClickable(WebElement element, int tiempo) {
 		WebDriverWait wait;
 		wait = new WebDriverWait(driver, tiempo);
 		wait.until(ExpectedConditions.elementToBeClickable(element));
 	}
 	public void espera(int espera) {
 		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
 	}
 	
 public void cargando(int loadingPage) {
 		
 		//espera a que salga el elemento cargando
 				try {
 		    		waitForElementToDisappear(By.id("general__spinner"), 300, driver);
 		    	} catch (Exception e) {
 		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
 		    	}
 		 
 	}
 	 
 	/*
 	 ***************WEBELEMENTS*********** 
 	 */

 	@FindBy(how = How.CLASS_NAME,using = "form-control")
 	private List<WebElement> campos;

 	@FindBy(how = How.ID,using = "ACA VA EL ID DEL BTN CONTINUAR")
 	private WebElement btnContinuar; //luego del web element dar el nombre que se desee al id para poder identificarlo facilmente

 	@FindBy(how = How.XPATH,using = "ACA VA EL XPATH")
 	private WebElement horaInicialRecepcion; //luego del web element dar el nombre que se desee al XPATH para poder identificarlo facilmente
 	


 	
 	
 	//************CONSTRUCTOR***********
 		
 	@FindBy(how = How.ID,using = "email_reg")
 	private WebElement EmailnombreUsuario;
 	
 	@FindBy(how = How.ID,using = "password_reg")
 	private WebElement contraseñaUsuario;
 	
 	@FindBy(how = How.ID,using = "container-chat-open-button")
 	private WebElement chatOnline;
 	
	@FindBy(how = How.ID,using = "nickname")
 	private WebElement pepe;

 	 
 	//***************************** 

 	 
 	public void LogInTodosGamers(String passAmbiente, String cuentaAcrear) {
 		
 		System.out.println();
 		System.out.println("***************************************************************************");
 		System.out.println();
 		System.out.println("Inicio de Test EDPTG01-Crear Cuenta");
 		 
 		cargando(500);
 		espera(1500);
 		
 		WebElement btnRegistrar = driver.findElement(By.cssSelector(".btn.btn-primary.btn_register"));
 		btnRegistrar.click();
 		
 		espera(2000);
 		
 		driver.switchTo();
 			EmailnombreUsuario.sendKeys(cuentaAcrear);
 			espera(500);
 			EmailnombreUsuario.sendKeys(Keys.TAB+"Crowley");
 			contraseñaUsuario.click();
 			contraseñaUsuario.sendKeys("123456");
 		
 			WebElement btnRegistrarse = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.login.g-recaptcha"));
 			btnRegistrarse.click();
 		driver.switchTo().defaultContent();
 		
 		cargando(500);
 		espera(1500);
 		
 		 
 		System.out.println("Resultado Esperado: Debera crearse una cuenta");
 		System.out.println();
 		System.out.println("Fin de Test EDPTG01 - Crear Cuenta");
 	}
 	
 	
 	
 }
  