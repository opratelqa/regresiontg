package pages;

 
import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseTG;
import org.openqa.selenium.support.Color;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
  
public class EDPTG123PassWordReset extends TestBaseTG {
	
	final WebDriver driver; 
	public EDPTG123PassWordReset(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	 
	
	  
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("general__spinner"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	 
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "ACA VA EL ID DEL BTN CONTINUAR")
	private WebElement btnContinuar; //luego del web element dar el nombre que se desee al id para poder identificarlo facilmente

	@FindBy(how = How.XPATH,using = "ACA VA EL XPATH")
	private WebElement horaInicialRecepcion; //luego del web element dar el nombre que se desee al XPATH para poder identificarlo facilmente

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "email_reg")
	private WebElement nombreUsuario;
	
	@FindBy(how = How.ID,using = "password_reg")
	private WebElement contraseñaUsuario;
	
	@FindBy(how = How.ID,using = "container-chat-open-button")
	private WebElement chatOnline;
	
	@FindBy(how = How.ID,using = "email")
	private WebElement emailUser;
	
	@FindBy(how = How.ID,using = "password")
	private WebElement NewPasswordUser;
	
	@FindBy(how = How.ID,using = "password-confirm")
	private WebElement ConfirmNewPassword;

	 
	//***************************** 

	 
	public void ResetStep1(String passAmbiente, String chooseYourUser) {		
		System.out.println();
		System.out.println("***************************************************************************");
		System.out.println();
		System.out.println("Inicio de Test EDPTG123 - PassWordReset");
		
		cargando(500);
		espera(1500); 
		  
		WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
		btnIngresar.click();
		
		cargando(500);
		espera(900); 
		
		WebElement restablecerContraseña = driver.findElement(By.xpath("//*[contains(text(), ' Resetear Contraseña')]"));
		restablecerContraseña.click();
		
		
		// EN INGLES
		//WebElement restablecerContraseña = driver.findElement(By.xpath("//*[contains(text(), ' Reset password')]"));
		//restablecerContraseña.click();
		
		cargando(500);
		espera(900);
		
		emailUser.sendKeys(chooseYourUser);
		
		WebElement btnEnviarClave = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.submit"));
		btnEnviarClave.click();
		
		espera(2500);
		
		WebElement alertaDeMensaje = driver.findElement(By.cssSelector(".alert.alert-success"));
		String mensaje = alertaDeMensaje.getText();
		System.out.println(mensaje);
		
		espera(2500);
		
		//agregar assert
		
	
	}
	
	public void ResetStep2(String passAmbiente, String chooseYourUser, String restauraPassword) {
		
		//driver.get("https://accounts.google.com");
		
		driver.get("https://accounts.google.com/signin/v2/identifier?flowName=GlifWebSignIn&flowEntry=ServiceLogin");
		
		espera(2500);
		
		   WebElement userElement = driver.findElement(By.id("identifierId"));                                                                                             
		   userElement.sendKeys(chooseYourUser);
		   
		   espera(2500);
		   
		   WebElement btnSiguiente = driver.findElement(By.id("identifierNext"));                                                                                             
		   btnSiguiente.click();
		   
		   espera(2500);
		   
		   
		   WebElement PassWordGmail = driver.findElement(By.name("password"));                                                                                             
		   PassWordGmail.sendKeys("tg_123456789_tg");
		    
		   espera(2500);
		  
		   
		   
		   WebElement btnSiguiente2 = driver.findElement(By.id("passwordNext"));                                                                                             
		   btnSiguiente2.click();
		   
		   espera(3000);
		   
		   driver.get("https://mail.google.com/mail/u/0/#inbox");
		   
		   espera(25000);
		   
		   driver.get("https://mail.google.com/mail/u/0/#inbox");
		   
		   espera(25000);
		   
		   driver.get("https://mail.google.com/mail/u/0/#inbox");
		   
		   espera(10000);
		 
		   String s = driver.findElement(By.xpath("//input[@placeholder='Buscar correo']")).getAttribute("placeholder");
		   System.out.println(s);
		   
		   WebElement barraBuscadora =driver.findElement(By.xpath("//input[@placeholder='Buscar correo']"));
		   barraBuscadora.sendKeys("Solicitud para restablecer contraseña" + Keys.ENTER);
		   
		   espera(9000);
		   
		   WebElement SelectEmail =driver.findElement(By.xpath("//*[@id=':1']/div/div[2]/div[4]/div[1]"));
		   SelectEmail.click();
		   
		   espera(2500);
		   
			
		   //driver.findElement(By.partialLinkText(restauraPassword)).click();	
		   
		   String urlRestablece = driver.findElement(By.partialLinkText(restauraPassword)).getText();
		   	espera(500);
		   		System.out.println(urlRestablece);
		   			driver.get(urlRestablece);
		   		cargando(500);
		   	espera(500);  
		   
		   /*List<WebElement> btnRestablece = driver.findElements(By.tagName("wbr")); 
	        int i;
	        espera(900);
	        		for(i = 0; i < btnRestablece.size(); i++) { 
	        			espera(900);
	        			if((btnRestablece.get(i).getText().toLowerCase()).contains(restauraPassword.toLowerCase()))	{                
	        				espera(900);
	        				btnRestablece.get(i).click();
							break;
	        		}
	        	}*/
		   
		   espera(9000); 
		   
	}
	
	public void ResetStep3(String passAmbiente, String chooseYourUser) {
		
		
		cargando(500);
		espera(2000);
		driver.findElement(By.id("email")).sendKeys(chooseYourUser);
		driver.findElement(By.id("password")).sendKeys("123456");
		driver.findElement(By.id("password-confirm")).sendKeys("123456");
		//emailUser.sendKeys(chooseYourUser);
		//NewPasswordUser.sendKeys("123456");
		//ConfirmNewPassword.sendKeys("123456");
		
		WebElement btnConfirmarPassWord = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.submit"));
		btnConfirmarPassWord.click();
		
	
		cargando(500);
		espera(500);	   
		
		//cierra el popover de vincula juegos
		
		
		WebElement vinculaJuegos = driver.findElement(By.cssSelector(".popover.fade.left.in"));
		vinculaJuegos.click();
		  
		 
		System.out.println("Resultado Esperado: Se debera resetear el Password del usuario");
		System.out.println();
		System.out.println("Fin de Test EDPTG123 - PassWord Reset");
		
	}
	
	
	 public void limpiaCasilla() {
		
		driver.get("https://mail.google.com");
		espera(2500);
		
		
		
		espera(5000);
		
	} 
	
	
	public void LogOutTodosGamers(String apuntaA) {	
		
		cargando(500); 
			espera(1500);
			WebElement btnLogOut = driver.findElement(By.cssSelector(".icon-logout"));
			btnLogOut.click();
			driver.get(apuntaA + "todosgamers.com/logout");
			cargando(500); 
		espera(500);	
		System.out.println("Log out");
	}	
	
}
 